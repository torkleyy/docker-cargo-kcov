# docker-cargo-kcov

Provides a docker image for producing line coverage using [`cargo-kcov`].

[`cargo-kcov`]: https://github.com/kennytm/cargo-kcov

## Usage

This image only installs everything; you need to run the commands yourself.

I'm using the image in detached mode to copy things back and forth. For that, we should
block the container like this:

```sh
docker run -dt --security-opt seccomp=unconfined --name mykcov1 --entrypoint /bin/sh registry.gitlab.com/torkleyy/docker-cargo-kcov
```

Now, we have a container running called `mykcov1`.

Next, copy all required files to its volume:

```sh
docker cp Cargo.toml mykcov1:/volume
docker cp Cargo.lock mykcov1:/volume
docker cp src/ mykcov1:/volume
```

Now we can generate the coverage report by running `cargo kcov` inside the container:

```sh
docker exec -t mykcov1 /bin/sh -c "cargo kcov"
```

This should store the coverage report to `/volume/target/cov`. Copy it over using `docker cp`:

```sh
docker cp mykcov1:/volume/target/cov ./this/is/where/I/want/my/coverage/report
```

Lastly, remove the container (`-f` is required since we're blocking it infinitely):

```sh
docker rm -f mykcov1
```
